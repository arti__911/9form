'use strict'

let infoUser = document.querySelectorAll('.worksheet-data__field input');
let skills = document.querySelectorAll('label[name="js"]');
let counter = document.querySelector('.worksheet-result__counter-num');
let spidometr = document.querySelector('.worksheet-result__scale svg');
console.log(spidometr)


infoUser.forEach(function (element) {
    function fieldFocus() {
        this.parentNode.classList.add('worksheet-data__field--active')
    }
    function fieldBlur() {
        if (this.value == '') {
            this.parentNode.classList.remove('worksheet-data__field--active')
        }
    }
    element.addEventListener('focus', fieldFocus);
    element.addEventListener('blur', fieldBlur);
})

skills.forEach(function (element) {
    element.onclick = function () {
        let countStringToNum = parseFloat(document.querySelector('.worksheet-result__counter-num').value);
        let skillIncrement = countStringToNum + 25;
        let skillDecrement = countStringToNum - 25;

        if (this.previousElementSibling.value == 'no') {
            this.previousElementSibling.value = 'yes';
            countStringToNum;
            counter.value = skillIncrement;
            let fiveSpidometr = + counter.value / 5;
            spidometr.style.transform = 'rotate(' + ((+ counter.value / 0.5) - fiveSpidometr) + 'deg )';
        }  else {
            this.previousElementSibling.value = 'no';
            countStringToNum
            counter.value = skillDecrement;
            let fiveSpidometr = + counter.value / 5;
            let spidometrIncrement = spidometr.style.transform = 'rotate(' + ((+ counter.value / 0.5) - fiveSpidometr) + 'deg )';
            spidometr.style.transform = 'rotate(' + (spidometrIncrement - 45) + 'deg )';
        }
    }
})