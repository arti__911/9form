'use strict'

let infoUser = document.querySelectorAll('.worksheet-data__field input');
let skills = document.querySelector('.worksheet-skills__skill input');
let counter = document.querySelector('.worksheet-result__counter-num');
let counterStep = 25;


infoUser.forEach(function (element) {
    function fieldFocus() {
        this.parentNode.classList.add('worksheet-data__field--active')
    }
    function fieldBlur() {
        if (this.value == '') {
            this.parentNode.classList.remove('worksheet-data__field--active')
        }
    }
    element.addEventListener('focus', fieldFocus);
    element.addEventListener('blur', fieldBlur);
})